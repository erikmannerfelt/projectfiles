import projectfiles
import pytest


def test_constant():

    class Constants(projectfiles.Constant):
        a: float = 1.
        b: int = 2.

    constants = Constants()


    with pytest.raises(ValueError):
        constants.a = 1
        constants["a"] = 1

    assert constants.__annotations__["a"] == float
    assert constants.__annotations__["b"] == int
        


    
