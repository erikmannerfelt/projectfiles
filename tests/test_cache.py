import projectfiles

import tempfile
from pathlib import Path
import os
import json


def test_get_checksum():
    assert projectfiles.caching.get_checksum([1, 2]) == projectfiles.caching.get_checksum([1, 2])
    assert projectfiles.caching.get_checksum([1, 2]) != projectfiles.caching.get_checksum([1, 3])


def test_cache():
    textfile = "blablabla"

    def string_func(number: int) -> str:
        """Return the predefined string plus a given number."""
        return textfile + str(number)

    def dict_func(number: int) -> dict[str, int]:
        return {"value": number}

    with tempfile.TemporaryDirectory() as temp_dir:
        cache_dir = Path(temp_dir)

        cached_func = projectfiles.cache(string_func, cache_dir=cache_dir, routine="text")

        assert cached_func.__doc__ == string_func.__doc__
        assert cached_func.__annotations__ == string_func.__annotations__

        retval = cached_func(1)

        cached_files = os.listdir(cache_dir)

        assert len(cached_files) == 1

        assert cached_files[0].endswith(".txt")
        assert retval == textfile + "1"
        assert retval == cached_func(1)

        cached_func2 = projectfiles.cache(string_func, cache_dir=cache_dir, routine="text:md")

        retval2 = cached_func2(2)

        assert retval2 == cached_func2(2)

        cached_files = os.listdir(cache_dir)

        assert len(cached_files) == 2

        assert len(list(filter(lambda p:".md" in p, cached_files))) == 1

        def json_saver(path, obj):
            with open(path, "w") as outfile:
                json.dump(obj, outfile)

        def json_loader(path):
            with open(path) as infile:
                return json.load(infile)

        cached_func3 = projectfiles.cache(
            dict_func, cache_dir=cache_dir, routine=(json_saver, json_loader, "json")
        )

        retval3 = cached_func3(3)

        cached_files = os.listdir(cache_dir)
        assert len(list(filter(lambda p: ".json" in p, cached_files))) == 1

        assert retval3 == cached_func3(3)


def test_get_cache_path():
    textfile = "blablabla"

    def string_func(number: int) -> str:
        return textfile + str(number)

    def func_with_default(number: int = 1):
        return string_func(number=number)

    with tempfile.TemporaryDirectory() as temp_dir:
        cache_dir = Path(temp_dir)

        cached_func = projectfiles.cache(string_func, cache_dir=cache_dir, routine="text")

        kwargs = {"number": 1}

        expected_path = projectfiles.get_cache_path(cached_func, kwargs=kwargs, routine="text", cache_dir=cache_dir)

        assert not expected_path.is_file()

        retval = cached_func(**kwargs)
        assert expected_path.is_file()

        with open(expected_path) as infile:
            assert infile.read() == retval

        cached_func2 = projectfiles.cache(func_with_default, cache_dir=cache_dir, routine="text")

        expected_path2 = projectfiles.get_cache_path(cached_func2, routine="text", cache_dir=cache_dir)

        assert not expected_path2.is_file()
        # Check that there is a checksum after the function name
        assert len(expected_path2.stem) > (len("func_with_default") + 20)

        cached_func2()
        retval = cached_func2()

        assert retval == func_with_default()


def test_labelling():
    def func(val, name = None):
        return str(val)

    with tempfile.TemporaryDirectory() as temp_dir:
        cache_dir = Path(temp_dir)

        cached_func = projectfiles.cache(func, cache_dir=cache_dir, routine="text", cache_label="name")

        cached_func(1)
        cached_files = os.listdir(cache_dir)

        assert len(cached_files) == 1
        assert cached_files[0].startswith("func-"), cached_files[0]

        cached_func(2, name="hello")
        cached_files = os.listdir(cache_dir)
        assert len(cached_files) == 2

        assert len(list(filter(lambda f: "hello-func" in f, cached_files))) == 1


def test_cache_with_kwargs():
    def inner(val):
        return str(val)

    with tempfile.TemporaryDirectory() as temp_dir:
        cache_dir = Path(temp_dir)

        # Test the decorator without any argument
        @projectfiles.cache
        def func1(val: int):
            """Here's a docstring."""
            return inner(val)

        assert func1.__doc__ == "Here's a docstring."
        assert func1.__annotations__ == {"val": int}

        assert func1(3) == "3"
        assert func1(3) == "3"

        # Test the decorator with arguments
        @projectfiles.cache(cache_dir=cache_dir)
        def func2(val: int):
            """Here's another docstring."""
            return inner(val)

        assert func2.__doc__ == "Here's another docstring."
        assert func2.__annotations__ == {"val": int}

        assert func2(3) == func1(3)
        assert func2(3) == func1(3)

        # It's needed to test func1 without an argument, meaning a cache dir is created in the local directory.
        # It should be removed after the test.
        cache_path = projectfiles.caching.get_cache_path(func1, [3])
        if cache_path.is_file():
            os.remove(cache_path)
            if os.listdir(cache_path.parent) == 0:
                os.removedirs(cache_path.parent)


def test_skip_args():
    def inner(val, val2):
        return str(val) + str(val2)

    with tempfile.TemporaryDirectory() as temp_dir:
        cache_dir = Path(temp_dir)

        cached1_kwargs = {"cache_dir": cache_dir}
        cached1 = projectfiles.cache(inner, **cached1_kwargs)

        cached2_kwargs = cached1_kwargs | {"skip_args": ["val2"]}
        cached2 = projectfiles.cache(inner, **cached2_kwargs)

        # Check that the same cache path is given for the same arguments
        assert projectfiles.get_cache_path(cached1, args=[1, 2], **cached1_kwargs) == projectfiles.get_cache_path(
            cached1, args=[1, 2], **cached1_kwargs
        )
        # Check that different cache paths are given for different arguments
        assert projectfiles.get_cache_path(cached1, args=[1, 2], **cached1_kwargs) != projectfiles.get_cache_path(
            cached1, args=[1, 3], **cached1_kwargs
        )

        # Check that cached1 and cached2 become different as cached2 should skip val2
        assert projectfiles.get_cache_path(cached1, [1, 2], **cached1_kwargs) != projectfiles.get_cache_path(
            cached2, [1, 2], **cached2_kwargs
        )
        # Check that the same first argument but different second is the same as val2 should be skipped.
        assert projectfiles.get_cache_path(cached2, [1, 2], **cached2_kwargs) == projectfiles.get_cache_path(
            cached2, [1, 3], **cached2_kwargs
        )
