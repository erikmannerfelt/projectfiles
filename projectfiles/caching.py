from typing import Any, Callable, Literal
import hashlib
from pathlib import Path
import functools
import pickle
import inspect

def get_checksum(objects: list[Any]) -> str:
    return hashlib.sha256("".join(map(str, objects)).encode()).hexdigest()

class ChangeFlag:
    def __init__(self, init: bool = True):
        self.changed = init

def cache(func: Callable[list[Any], Any] | None = None, disable: bool = False, cache_dir: Path | str | None = None, skip_args: list[str] | None = None, routine: str | tuple[Callable[tuple[Path, Any], None], Callable[tuple[Path], Any], str | Path] = "pickle", cache_label: str | None = None):
    """
    Cache a function in a specified or default (.cache) directory.

    A custom routine can be specified. In that case, an extension, saving and loading function need to be given as a tuple: (extension, save, load)

    def load(path) -> Any: ..
    def save(path, obj) -> None: ...

    Arguments
    ---------
    - func: The function to cache (not needed when used as a decorator).
    - disable: Disable caching.
    - cache_dir: The directory to use as the cache. Defaults to "./.cache"
    - skip_args: Skip the following arguments in the argument checksum creation.
    - routine: The caching routine (pickle, text, text:csv, text:*) or a custom routine.
    - cache_label: An argument or kwarg whose value acts as a prefix to the caching name, e.g. "project1" -> ".cache/project1-func-85gfhad..."
    """
    def decorator(_func):
        @functools.wraps(_func)
        def inner(*args, **kwargs):

            cache_path, save_func, load_func = _get_cache_path_and_funcs(
                func=_func,
                args=args,
                kwargs=kwargs,
                cache_dir=cache_dir,
                skip_args=skip_args,
                routine=routine,
                cache_label=cache_label,
            )

            if cache_path.is_file():
                return load_func(cache_path)

            retval = _func(*args, **kwargs)

            cache_path.parent.mkdir(exist_ok=True, parents=True)

            save_func(cache_path, retval)
            return retval
        return inner

    if func is None:
        return decorator
    else:
        return decorator(func)
        
        

def _get_cache_path_and_funcs(func: Callable[list[Any], Any], args: list[Any] | None = None, kwargs: dict[str, Any] | None = None, cache_dir: Path | str | None = None, skip_args: list[str] | None = None, routine: str | tuple[Callable[tuple[Path, Any], None], Callable[tuple[Path], Any], str | Path] = "pickle", cache_label: str | None = None) -> tuple[Path, Callable[tuple[Path, Any], Any], Callable[tuple[Path], Any]]:
    if cache_dir is None:
        cache_dir = Path(".cache")
    cache_dir = Path(cache_dir)
    if isinstance(routine, str):
        if routine == "pickle":
            extension = "pkl"
            def load_func(path):
                with open(path, "rb") as infile:
                    return pickle.load(infile)
            def save_func(path, obj):
                with open(path, "wb") as outfile:
                    pickle.dump(obj, outfile)

        elif routine.startswith("text"):
            parts = routine.split(":")
            if len(parts) > 1:
                extension = parts[-1]
            else:
                extension = "txt"

            def load_func(path):
                with open(path) as infile:
                    return infile.read()
            def save_func(path, obj):
                with open(path, "w") as outfile:
                    outfile.write(obj)
                
        else:
            raise ValueError(f"Unknown cache format: {routine}")
    elif isinstance(routine, tuple):
        save_func, load_func, extension = routine
    else:
        raise ValueError("Routine argument not understood.")


    if args is None:
        args = []
    if kwargs is None:
        kwargs = {}
    if skip_args is None:
        skip_args = []
    
    bound = inspect.signature(func).bind(*args, **kwargs)
    bound.apply_defaults()

    if cache_label is not None and bound.arguments.get(cache_label, None) is not None:
        try:
            label = str(bound.arguments[cache_label]) + "-"
        except KeyError as exception:
            raise ValueError(f"{cache_label=} provided but it does not match a function arg/kwarg.") from exception
    else:
        label = ""
        
    to_checksum = [v for k, v in bound.arguments.items() if k not in skip_args]

    if len(to_checksum) > 0:
        checksum = "-" + get_checksum(to_checksum)
    else:
        checksum = ""

    cache_path = cache_dir.joinpath(f"{label}{func.__name__}{checksum}.{extension}")
    return cache_path, save_func, load_func

def get_cache_path(func: Callable[list[Any], Any], args: list[Any] | None = None, kwargs: dict[str, Any] | None = None, cache_dir: Path | str | None = None, skip_args: list[str] | None = None, routine: str | tuple[Callable[tuple[Path, Any], None], Callable[tuple[Path], Any], str | Path] = "pickle", cache_label: str | None = None) -> Path:
    """
    Get the caching path that would be created using the @cache decorator.

    Arguments
    ---------
    - func: The function to cache.
    - cache_dir: The directory to use as the cache. Defaults to "./.cache"
    - skip_args: Skip the following arguments in the argument checksum creation.
    - routine: The caching routine (pickle, text, text:csv, text:*) or a custom routine.
    - cache_label: An argument or kwarg whose value acts as a prefix to the caching name, e.g. "project1" -> ".cache/project1-func-85gfhad..."
    """
    return _get_cache_path_and_funcs(
        func=func,
        args=args,
        kwargs=kwargs,
        cache_dir=cache_dir,
        skip_args=skip_args,
        routine=routine,
        cache_label=cache_label
    )[0]
